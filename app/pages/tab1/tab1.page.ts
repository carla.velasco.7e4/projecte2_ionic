import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})

export class Tab1Page {

  movies = [];

  constructor(private data: DataService) {}


  ngOnInit(): void {
    this.loadMovies();
  }

  loadMovies (event?) {
    this.data.getMovies().subscribe(
      resp => {
        console.log('Movies', resp);
        if(resp.page === null) {
          event.target.disabled = true;
          event.target.complete();
          return;
        }
        this.movies.push(...resp.results);
        if(event) {
          event.target.complete();
        }
      }
    )
  }

  loadData(event?) {
    this.loadMovies(event);
  }
}
