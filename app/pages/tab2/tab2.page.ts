import { Component } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})

export class Tab2Page {

  movies = [];

  constructor(private data: DataService) {}

  ngOnInit(): void {
  }

  loadMovies (event?) {
    this.movies = [];
    var searched = (<HTMLInputElement>document.getElementById('searchbox')).value;
    this.data.getSearch(searched).subscribe(
      resp => {
        console.log('Search', resp);
        if(resp.page === null) {
          event.target.disabled = true;
          event.target.complete();
          return;
        }
        this.movies.push(...resp.results);
        if(event) {
          event.target.complete();
        }
      }
    )
  }

  loadData(event?) {
    this.loadMovies(event);

  }
}
