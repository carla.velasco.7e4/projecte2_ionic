import { Component } from '@angular/core';
import { DataLocalService } from 'src/app/services/data-local.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page {

  movies = [];
  
  constructor(private dataService: DataLocalService, private data: DataService) {
    this.loadData();
  }

  async loadData() {
    this.movies = await this.dataService.getData();
  }

 /*  async addData(movie) {
    await this.dataService.addData(movie)
    this.loadData;
  }

  async removeItem(movie) {
    this.dataService.removeItem(movie);
    this.movies.splice(movie,1);
  } */

}
