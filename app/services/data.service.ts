import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { RespuestaMovies } from '../interfaces/interfaces';
import { RootObject } from '../interfaces/intMovie';


@Injectable({
  providedIn: 'root'
})
export class DataService {

  key = "d7b626df56b785b0c4ff3c627a66adbe";
  page = 0;
  pageSearch = 1;
  lastQuery = "";

  constructor(private http: HttpClient) { }

  getMovies() {
    this.page++;
    return this.http.get<RespuestaMovies>(`https://api.themoviedb.org/3/movie/popular?api_key=${this.key}&page=${this.page}`);
  }

  getSingleMovie(id: number) {
    return this.http.get<RootObject>(`https://api.themoviedb.org/3/movie/${id}?api_key=${this.key}`); 
  }

  getSearch(query: string) {
    if(this.lastQuery == query) {
      this.pageSearch++;
    } else {
      this.pageSearch = 1;
    }
    this.lastQuery = query;
     return this.http.get<RespuestaMovies>(`https://api.themoviedb.org/3/search/movie?query=${query}&api_key=${this.key}`);
  }
}

