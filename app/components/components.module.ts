import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MoviesComponent } from './movies/movies.component';
import { IonicModule } from '@ionic/angular';


@NgModule({
  declarations: [
    MoviesComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
  ],
  exports: [
    MoviesComponent
  ]
})
export class ComponentsModule { }

