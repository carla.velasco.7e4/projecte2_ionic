import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { RootObject } from 'src/app/interfaces/intMovie';
import { DataLocalService } from 'src/app/services/data-local.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.component.html',
  styleUrls: ['./detalle.component.scss'],
})

export class DetalleComponent implements OnInit {

  movie: RootObject; 
  isFavourite = false;
  numero = 1;
  showCity = true;

  @Input() id;

  moviesFav = [];

  constructor(private modalCtrl: ModalController, private data: DataService, private dataService: DataLocalService) { }

  ngOnInit() {
    this.getData();
    this.checkFavourite();
    this.data.getSingleMovie(this.id).subscribe(
      resp => {
        this.movie = resp;
        if(this.movie.original_language == "en") {
          this.movie.original_language = "https://m.media-amazon.com/images/I/71qpamVBnLL._AC_SL1500_.jpg";
        } else if(this.movie.original_language == "fr"){
          this.movie.original_language = "https://upload.wikimedia.org/wikipedia/commons/c/c3/Flag_of_France.svg";
        } else if(this.movie.original_language == "de"){
          this.movie.original_language = "https://upload.wikimedia.org/wikipedia/commons/b/ba/Flag_of_Germany.svg";
        } else if(this.movie.original_language == "es"){
          this.movie.original_language = "https://upload.wikimedia.org/wikipedia/commons/8/89/Bandera_de_Espa%C3%B1a.svg";
        } else if(this.movie.original_language == "it"){
          this.movie.original_language = "https://upload.wikimedia.org/wikipedia/commons/0/03/Flag_of_Italy.svg";
        } else if(this.movie.original_language == "ja"){
          this.movie.original_language = "https://upload.wikimedia.org/wikipedia/commons/9/9e/Flag_of_Japan.svg";
        }
      }
    );
  }

  regresar(){
    this.modalCtrl.dismiss()
  }

  async getData() {
    this.moviesFav = await this.dataService.getData();
  }

  async checkFavourite() {
    var heart = document.getElementById('heart');
    await this.getData();
    for (let i = 0; i < this.moviesFav.length; i++) {
      const element = this.moviesFav[i];
       if (element.id == this.movie.id) {
          heart.setAttribute("name", "heart");
       }     
    }
    return this.isFavourite = false;
  }
  
  async addData(movie) {
    await this.dataService.addData(movie);
    this.getData;
  }

  async removeItem(index) {
    this.dataService.removeItem(index);
    this.moviesFav.splice(index,1);
  }

  async favorito(movie) {
    var heart = document.getElementById('heart');
    await this.getData();
    for (let i = 0; i < this.moviesFav.length; i++) {
      const element = this.moviesFav[i];
      if(element.id == movie.id) {
        this.isFavourite = false;
        heart.setAttribute("name", "heart-outline");
        this.removeItem(i);
        return  
      } 
    }
    this.addData(movie); 
    heart.setAttribute("name", "heart");
  }
  
}
